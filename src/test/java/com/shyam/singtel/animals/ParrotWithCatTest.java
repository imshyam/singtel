package com.shyam.singtel.animals;

        import com.shyam.singtel.animals.birds.Parrot;
        import com.shyam.singtel.enums.Messages;
        import com.shyam.singtel.enums.Voices;
        import org.junit.After;
        import org.junit.Before;
        import org.junit.Test;

        import java.io.ByteArrayOutputStream;
        import java.io.PrintStream;

        import static org.junit.Assert.assertEquals;

public class ParrotWithCatTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalFly = System.out;
    Parrot parrot;

    @Before
    public void setUpStreams() {
        parrot = new Parrot(Voices.CAT);
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalFly);
    }

    @Test
    public void walk() {
        System.out.println(parrot.getClass().getSimpleName() + " " + parrot.getWalk().walk());
        assertEquals("Parrot " + Messages.WALK.getValue() + "\n", outContent.toString());
    }

    @Test
    public void fly() {
        System.out.println(parrot.getClass().getSimpleName() + " " + parrot.getFly().fly());
        assertEquals("Parrot " + Messages.FLY.getValue() + "\n", outContent.toString());
    }

    @Test
    public void sing() {
        System.out.println(parrot.getClass().getSimpleName() + " " + parrot.getSing().sing());
        assertEquals("Parrot " + Messages.SING.getValue() + Voices.CAT.getVoice() + "\n", outContent.toString());
    }

}
