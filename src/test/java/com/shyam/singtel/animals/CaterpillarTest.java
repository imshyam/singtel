package com.shyam.singtel.animals;

import com.shyam.singtel.enums.Messages;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class CaterpillarTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalFly = System.out;
    Caterpillar caterpillar;

    @Before
    public void setUpStreams() {
        caterpillar = new Caterpillar();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalFly);
    }

    @Test
    public void walk() {
        System.out.println(caterpillar.getClass().getSimpleName() + " " + caterpillar.getWalk().walk());
        assertEquals("Caterpillar " + Messages.WALK.getValue() + "\n", outContent.toString());
    }

}
