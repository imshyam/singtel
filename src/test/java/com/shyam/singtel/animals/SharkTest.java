package com.shyam.singtel.animals;

import com.shyam.singtel.animals.fish.Shark;
import com.shyam.singtel.enums.FishColor;
import com.shyam.singtel.enums.FishSize;
import com.shyam.singtel.enums.Messages;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class SharkTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalFly = System.out;
    Shark shark;

    @Before
    public void setUpStreams() {
        shark = new Shark();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalFly);
    }

    @Test
    public void killFish() {
        System.out.println(shark.getClass().getSimpleName() + " " + shark.getKillFish().killFish());
        assertEquals("Shark " + Messages.EAT_OTHER_FISH.getValue() + "\n", outContent.toString());
    }

    @Test
    public void joke() {
        System.out.println(shark.getClass().getSimpleName() + " " + shark.getJoke().makeJoke());
        assertEquals("Shark " + Messages.CANT_JOKE.getValue() + "\n", outContent.toString());
    }

    @Test
    public void specs() {
        assertEquals(shark.getSize(), FishSize.SHARK.getValue());
        assertEquals(shark.getColor(), FishColor.SHARK.getValue());
    }

}
