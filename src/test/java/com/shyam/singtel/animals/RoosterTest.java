package com.shyam.singtel.animals;

import com.shyam.singtel.animals.birds.Rooster;
import com.shyam.singtel.enums.Messages;
import com.shyam.singtel.enums.Voices;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class RoosterTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalFly = System.out;
    Rooster rooster;

    @Before
    public void setUpStreams() {
        rooster = new Rooster();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalFly);
    }

    @Test
    public void walk() {
        System.out.println(rooster.getClass().getSimpleName() + " " + rooster.getWalk().walk());
        assertEquals("Rooster " + Messages.WALK.getValue() + "\n", outContent.toString());
    }

    @Test
    public void fly() {
        System.out.println(rooster.getClass().getSimpleName() + " " + rooster.getFly().fly());
        assertEquals("Rooster " + Messages.CANT_FLY.getValue() + "\n", outContent.toString());
    }

    @Test
    public void sing() {
        System.out.println(rooster.getClass().getSimpleName() + " " + rooster.getSing().sing());
        assertEquals("Rooster " + Messages.SING.getValue() + Voices.ROOSTER.getVoice() + "\n", outContent.toString());
    }

}
