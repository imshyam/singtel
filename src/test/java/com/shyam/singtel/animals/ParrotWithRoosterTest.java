package com.shyam.singtel.animals;

import com.shyam.singtel.animals.birds.Parrot;
import com.shyam.singtel.enums.Messages;
import com.shyam.singtel.enums.Voices;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class ParrotWithRoosterTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalFly = System.out;
    Parrot parrot;

    @Before
    public void setUpStreams() {
        parrot = new Parrot(Voices.ROOSTER);
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalFly);
    }

    @Test
    public void sing() {
        System.out.println(parrot.getClass().getSimpleName() + " " + parrot.getSing().sing());
        assertEquals("Parrot " + Messages.SING.getValue() + Voices.ROOSTER.getVoice() + "\n", outContent.toString());
    }

}
