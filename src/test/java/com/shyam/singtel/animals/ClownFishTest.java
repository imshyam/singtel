package com.shyam.singtel.animals;

import com.shyam.singtel.animals.fish.Clownfish;
import com.shyam.singtel.enums.FishColor;
import com.shyam.singtel.enums.FishSize;
import com.shyam.singtel.enums.Messages;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class ClownFishTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalFly = System.out;
    Clownfish clownFish;

    @Before
    public void setUpStreams() {
        clownFish = new Clownfish();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalFly);
    }

    @Test
    public void killFish() {
        System.out.println(clownFish.getClass().getSimpleName() + " " + clownFish.getKillFish().killFish());
        assertEquals("Clownfish " + Messages.CANT_EAT_OTHER_FISH.getValue() + "\n", outContent.toString());
    }

    @Test
    public void joke() {
        System.out.println(clownFish.getClass().getSimpleName() + " " + clownFish.getJoke().makeJoke());
        assertEquals("Clownfish " + Messages.JOKE.getValue() + "\n", outContent.toString());
    }

    @Test
    public void specs() {
        assertEquals(clownFish.getSize(), FishSize.CLOWNFISH.getValue());
        assertEquals(clownFish.getColor(), FishColor.CLOWNFISH.getValue());
    }

}
