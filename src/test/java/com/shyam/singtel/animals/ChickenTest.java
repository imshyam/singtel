package com.shyam.singtel.animals;

import com.shyam.singtel.animals.birds.Chicken;
import com.shyam.singtel.enums.Messages;
import com.shyam.singtel.enums.Voices;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class ChickenTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalFly = System.out;
    Chicken chicken;

    @Before
    public void setUpStreams() {
        chicken = new Chicken();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalFly);
    }

    @Test
    public void walk() {
        System.out.println(chicken.getClass().getSimpleName() + " " + chicken.getWalk().walk());
        assertEquals("Chicken " + Messages.WALK.getValue() + "\n", outContent.toString());
    }

    @Test
    public void fly() {
        System.out.println(chicken.getClass().getSimpleName() + " " + chicken.getFly().fly());
        assertEquals("Chicken " + Messages.CANT_FLY.getValue() + "\n", outContent.toString());
    }

    @Test
    public void sing() {
        System.out.println(chicken.getClass().getSimpleName() + " " + chicken.getSing().sing());
        assertEquals("Chicken " + Messages.SING.getValue() + Voices.CHICKEN.getVoice() + "\n", outContent.toString());
    }

}
