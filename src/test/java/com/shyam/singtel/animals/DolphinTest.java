package com.shyam.singtel.animals;

import com.shyam.singtel.animals.fish.Dolphin;
import com.shyam.singtel.enums.Messages;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class DolphinTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalFly = System.out;
    Dolphin dolphin;

    @Before
    public void setUpStreams() {
        dolphin = new Dolphin();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalFly);
    }

    @Test
    public void swim() {
        System.out.println(dolphin.getClass().getSimpleName() + " " + dolphin.getSwim().swim());
        assertEquals("Dolphin " + Messages.SWIM.getValue() + "\n", outContent.toString());
    }

}
