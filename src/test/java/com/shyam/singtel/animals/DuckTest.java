package com.shyam.singtel.animals;

import com.shyam.singtel.animals.birds.Duck;
import com.shyam.singtel.enums.Messages;
import com.shyam.singtel.enums.Voices;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class DuckTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalFly = System.out;
    Duck duck;

    @Before
    public void setUpStreams() {
        duck = new Duck();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalFly);
    }

    @Test
    public void walk() {
        System.out.println(duck.getClass().getSimpleName() + " " + duck.getWalk().walk());
        assertEquals("Duck " + Messages.WALK.getValue() + "\n", outContent.toString());
    }

    @Test
    public void fly() {
        System.out.println(duck.getClass().getSimpleName() + " " + duck.getFly().fly());
        assertEquals("Duck " + Messages.FLY.getValue() + "\n", outContent.toString());
    }

    @Test
    public void sing() {
        System.out.println(duck.getClass().getSimpleName() + " " + duck.getSing().sing());
        assertEquals("Duck " + Messages.SING.getValue() + Voices.DUCK.getVoice() + "\n", outContent.toString());
    }

    @Test
    public void swim() {
        System.out.println(duck.getClass().getSimpleName() + " " + duck.getSwim().swim());
        assertEquals("Duck " + Messages.SWIM.getValue() + "\n", outContent.toString());
    }

}
