package com.shyam.singtel.animals;

import com.shyam.singtel.animals.birds.Butterfly;
import com.shyam.singtel.enums.Messages;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class ButterflyTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalFly = System.out;
    Butterfly butterfly;

    @Before
    public void setUpStreams() {
        butterfly = new Butterfly();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalFly);
    }

    @Test
    public void fly() {
        System.out.println(butterfly.getClass().getSimpleName() + " " + butterfly.getFly().fly());
        assertEquals("Butterfly " + Messages.FLY.getValue() + "\n", outContent.toString());
    }

    @Test
    public void sing() {
        System.out.println(butterfly.getClass().getSimpleName() + " " + butterfly.getSing().sing());
        assertEquals("Butterfly " + Messages.CANT_SING.getValue() + "\n", outContent.toString());
    }

}
