package com.shyam.singtel;

import com.shyam.singtel.enums.Messages;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class BirdTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalFly = System.out;
    Bird bird;

    @Before
    public void setUpStreams() {
        bird = new Bird();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalFly);
    }

    @Test
    public void walk() {
        System.out.println(bird.getClass().getSimpleName() + " " + bird.getWalk().walk());
        assertEquals("Bird " + Messages.WALK.getValue() + "\n", outContent.toString());
    }

    @Test
    public void fly() {
        System.out.println(bird.getClass().getSimpleName() + " " + bird.getFly().fly());
        assertEquals("Bird " + Messages.FLY.getValue() + "\n", outContent.toString());
    }


    @Test
    public void sing() {
        System.out.println(bird.getClass().getSimpleName() + " " + bird.getSing().sing());
        assertEquals("Bird " + Messages.SING.getValue() + "\n", outContent.toString());
    }

}
