package com.shyam.singtel;

import com.shyam.singtel.enums.Messages;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class FishTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalFly = System.out;
    Fish fish;

    @Before
    public void setUpStreams() {
        fish = new Fish();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalFly);
    }

    @Test
    public void swim() {
        System.out.println(fish.getClass().getSimpleName() + " " + fish.getSwim().swim());
        assertEquals("Fish " + Messages.SWIM.getValue() + "\n", outContent.toString());
    }

    @Test
    public void cantWalk() {
        System.out.println(fish.getClass().getSimpleName() + " " + fish.getWalk().walk());
        assertEquals("Fish " + Messages.CANT_WALK.getValue() + "\n", outContent.toString());
    }

    @Test
    public void cantSing() {
        System.out.println(fish.getClass().getSimpleName() + " " + fish.getSing().sing());
        assertEquals("Fish " + Messages.CANT_SING.getValue() + "\n", outContent.toString());
    }

}
