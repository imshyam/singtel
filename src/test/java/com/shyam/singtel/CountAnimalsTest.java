package com.shyam.singtel;

import com.shyam.singtel.animals.animal.Cat;
import com.shyam.singtel.animals.animal.Dog;
import com.shyam.singtel.animals.animal.Frog;
import com.shyam.singtel.animals.birds.*;
import com.shyam.singtel.animals.fish.Clownfish;
import com.shyam.singtel.animals.fish.Dolphin;
import com.shyam.singtel.animals.fish.Shark;
import com.shyam.singtel.enums.Voices;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class CountAnimalsTest {

    Animal[] animals;
    CountAnimals countAnimals;

    @Before
    public void setUpStreams() {
        countAnimals = new CountAnimals();
        animals = new Animal[]{
                new Bird(),
                new Duck(),
                new Chicken(),
                new Rooster(),
                new Parrot(Voices.CAT),
                new Fish(),
                new Shark(),
                new Clownfish(),
                new Dolphin(),
                new Frog(),
                new Dog(),
                new Butterfly(),
                new Cat()
        };
    }

    @Test
    public void countFlying() {
        assertEquals(4, countAnimals.countFlying(animals));
        assertEquals(9, countAnimals.countWalking(animals));
        assertEquals(8, countAnimals.countSinging(animals));
        assertEquals(6, countAnimals.countSwimming(animals));
    }
}