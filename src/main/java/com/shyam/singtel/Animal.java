package com.shyam.singtel;

import com.shyam.singtel.abilities.fly.Fly;
import com.shyam.singtel.abilities.joke.Joke;
import com.shyam.singtel.abilities.kill.KillFish;
import com.shyam.singtel.abilities.sing.Sing;
import com.shyam.singtel.abilities.swim.Swim;
import com.shyam.singtel.abilities.walk.Walk;

public abstract class Animal {
    protected Walk walk;
    protected Fly fly;
    protected Sing sing;
    protected Swim swim;
    protected Joke joke;
    protected KillFish killFish;

    public Walk getWalk() {
        return walk;
    }

    public void setWalk(Walk walk) {
        this.walk = walk;
    }

    public Fly getFly() {
        return fly;
    }

    public void setFly(Fly fly) {
        this.fly = fly;
    }

    public Sing getSing() {
        return sing;
    }

    public void setSing(Sing sing) {
        this.sing = sing;
    }

    public Swim getSwim() {
        return swim;
    }

    public void setSwim(Swim swim) {
        this.swim = swim;
    }

    public Joke getJoke() {
        return joke;
    }

    public void setJoke(Joke joke) {
        this.joke = joke;
    }

    public KillFish getKillFish() {
        return killFish;
    }

    public void setKillFish(KillFish killFish) {
        this.killFish = killFish;
    }
}
