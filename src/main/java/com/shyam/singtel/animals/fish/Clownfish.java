package com.shyam.singtel.animals.fish;

import com.shyam.singtel.Fish;
import com.shyam.singtel.abilities.joke.CanJoke;
import com.shyam.singtel.abilities.kill.CantKillFish;
import com.shyam.singtel.enums.FishColor;
import com.shyam.singtel.enums.FishSize;

public class Clownfish extends Fish {

    String size;
    String color;

    public Clownfish() {
        this.joke = new CanJoke();
        this.killFish = new CantKillFish();
        this.size = FishSize.CLOWNFISH.getValue();
        this.color = FishColor.CLOWNFISH.getValue();
    }

    public String getSize() {
        return size;
    }

    public String getColor() {
        return color;
    }
}
