package com.shyam.singtel.animals.fish;

import com.shyam.singtel.Animal;
import com.shyam.singtel.abilities.swim.CanSwim;

public class Dolphin extends Animal {
    public Dolphin() {
        this.swim = new CanSwim();
    }
}
