package com.shyam.singtel.animals.fish;

import com.shyam.singtel.Fish;
import com.shyam.singtel.abilities.joke.CantJoke;
import com.shyam.singtel.abilities.kill.CanKillFish;
import com.shyam.singtel.enums.FishColor;
import com.shyam.singtel.enums.FishSize;

public class Shark extends Fish {

    String size;
    String color;

    public Shark() {
        this.killFish = new CanKillFish();
        this.joke = new CantJoke();
        this.size = FishSize.SHARK.getValue();
        this.color = FishColor.SHARK.getValue();
    }

    public String getSize() {
        return size;
    }

    public String getColor() {
        return color;
    }

}
