package com.shyam.singtel.animals;

import com.shyam.singtel.Animal;
import com.shyam.singtel.abilities.walk.CanWalk;

public class Caterpillar extends Animal {
    public Caterpillar() {
        this.walk = new CanWalk();
    }
}
