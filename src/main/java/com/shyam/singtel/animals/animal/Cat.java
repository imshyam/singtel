package com.shyam.singtel.animals.animal;

import com.shyam.singtel.Animal;
import com.shyam.singtel.abilities.sing.CanSing;
import com.shyam.singtel.abilities.walk.CanWalk;
import com.shyam.singtel.enums.Voices;

public class Cat extends Animal {
    public Cat() {
        this.walk = new CanWalk();
        this.sing = new CanSing(Voices.CAT.getVoice());
    }
}
