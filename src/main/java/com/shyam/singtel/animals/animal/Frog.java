package com.shyam.singtel.animals.animal;

import com.shyam.singtel.Animal;
import com.shyam.singtel.abilities.sing.CanSing;
import com.shyam.singtel.abilities.swim.CanSwim;
import com.shyam.singtel.abilities.walk.CanWalk;
import com.shyam.singtel.enums.Voices;

public class Frog extends Animal {
    public Frog() {
        this.walk = new CanWalk();
        this.swim = new CanSwim();
        this.sing = new CanSing(Voices.FROG.getVoice());
    }
}
