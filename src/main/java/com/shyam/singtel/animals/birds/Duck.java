package com.shyam.singtel.animals.birds;

import com.shyam.singtel.Bird;
import com.shyam.singtel.abilities.sing.CanSing;
import com.shyam.singtel.abilities.swim.CanSwim;
import com.shyam.singtel.enums.Voices;

public class Duck extends Bird {
    public Duck() {
        this.swim = new CanSwim();
        this.sing = new CanSing(Voices.DUCK.getVoice());
    }
}
