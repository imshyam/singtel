package com.shyam.singtel.animals.birds;

import com.shyam.singtel.Bird;
import com.shyam.singtel.abilities.sing.CanSing;
import com.shyam.singtel.enums.Voices;

public class Parrot extends Bird {

    public Parrot(Voices voices) {
        switch (voices) {
            case CAT:
                this.sing = new CanSing(Voices.CAT.getVoice());
                break;
            case DOG:
                this.sing = new CanSing(Voices.DOG.getVoice());
                break;
            case ROOSTER:
                this.sing = new CanSing(Voices.ROOSTER.getVoice());
                break;
            default:
                break;
        }
    }

}
