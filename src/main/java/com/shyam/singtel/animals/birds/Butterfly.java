package com.shyam.singtel.animals.birds;

import com.shyam.singtel.Bird;
import com.shyam.singtel.abilities.sing.CantSing;

public class Butterfly extends Bird {
    public Butterfly() {
        this.sing = new CantSing();
    }
}
