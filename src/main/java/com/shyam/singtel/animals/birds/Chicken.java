package com.shyam.singtel.animals.birds;

import com.shyam.singtel.Bird;
import com.shyam.singtel.abilities.fly.CantFly;
import com.shyam.singtel.abilities.sing.CanSing;
import com.shyam.singtel.enums.Voices;

public class Chicken extends Bird {

    public Chicken() {
        this.sing = new CanSing(Voices.CHICKEN.getVoice());
        this.fly = new CantFly();
    }

}
