package com.shyam.singtel.animals.birds;

import com.shyam.singtel.abilities.sing.CanSing;
import com.shyam.singtel.enums.Voices;

public class Rooster extends Chicken{
    public Rooster() {
        this.sing = new CanSing(Voices.ROOSTER.getVoice());
    }
}
