package com.shyam.singtel;

import com.shyam.singtel.abilities.fly.CanFly;
import com.shyam.singtel.abilities.sing.CanSing;
import com.shyam.singtel.abilities.walk.CanWalk;

public class Bird extends Animal {

    public Bird() {
        this.walk = new CanWalk();
        this.fly = new CanFly();
        this.sing = new CanSing("");
    }

}
