package com.shyam.singtel.enums;

public enum FishSize {
    SHARK (1,"Big"),
    CLOWNFISH (2,"Small");

    private int id;
    private String value;

    FishSize(int id, String val) {
        this.id = id;
        this.value = val;
    }

    public String getValue() {
        return this.value;
    }
}
