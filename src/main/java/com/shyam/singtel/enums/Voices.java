package com.shyam.singtel.enums;

public enum Voices {
    CHICKEN(1, "Cluck, cluck"),
    DUCK(2, "Quack, quack"),
    ROOSTER(3, "Cock-a-doodle-doo"),
    DOG(4, "Woof, woof"),
    CAT(5, "Meow"),
    FROG(6, "Frog Voice");

    private int id;
    private String voice;

    Voices(int id, String voice) {
        this.id = id;
        this.voice = voice;
    }

    public String getVoice() {
        return this.voice;
    }
}
