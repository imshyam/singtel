package com.shyam.singtel.enums;

public enum FishColor {
    SHARK (1,"Grey"),
    CLOWNFISH (2,"Colorful(Orange)");

    private int id;
    private String value;

    FishColor(int id, String val) {
        this.id = id;
        this.value = val;
    }

    public String getValue() {
        return this.value;
    }
}
