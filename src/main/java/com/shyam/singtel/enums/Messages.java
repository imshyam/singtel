package com.shyam.singtel.enums;

public enum Messages {
    WALK (1,"is walking."),
    CANT_WALK (2,"can't walk."),
    FLY (3,"is flying."),
    CANT_FLY (4,"can't fly."),
    SWIM (5,"is swimming."),
    CANT_SWIM (6,"can't swim."),
    SING (7,"says: "),
    CANT_SING (8,"can't sing."),
    JOKE (9,"makes joke."),
    CANT_JOKE (10,"don't make Joke"),
    EAT_OTHER_FISH (11,"eat other fish."),
    CANT_EAT_OTHER_FISH (12,"don't eat other fish.");

    private int id;
    private String value;

    Messages(int id, String val) {
        this.id = id;
        this.value = val;
    }

    public String getValue() {
        return this.value;
    }
}
