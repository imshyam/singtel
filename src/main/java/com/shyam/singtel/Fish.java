package com.shyam.singtel;

import com.shyam.singtel.abilities.sing.CantSing;
import com.shyam.singtel.abilities.swim.CanSwim;
import com.shyam.singtel.abilities.walk.CantWalk;

public class Fish extends Animal {

    public Fish() {
        this.swim = new CanSwim();
        this.walk = new CantWalk();
        this.sing = new CantSing();
    }
}
