package com.shyam.singtel.abilities.joke;

import com.shyam.singtel.enums.Messages;

public class CantJoke implements Joke {
    public String makeJoke() {
        return Messages.CANT_JOKE.getValue();
    }
}
