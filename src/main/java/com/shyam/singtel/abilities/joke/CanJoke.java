package com.shyam.singtel.abilities.joke;

import com.shyam.singtel.enums.Messages;

public class CanJoke implements Joke {
    public String makeJoke() {
        return Messages.JOKE.getValue();
    }
}
