package com.shyam.singtel.abilities.fly;

import com.shyam.singtel.enums.Messages;

public class CantFly implements Fly {
    public String fly() {
        return Messages.CANT_FLY.getValue();
    }
}
