package com.shyam.singtel.abilities.fly;

import com.shyam.singtel.enums.Messages;

public class CanFly implements Fly {
    public String fly() {
        return Messages.FLY.getValue();
    }
}
