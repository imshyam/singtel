package com.shyam.singtel.abilities.swim;

import com.shyam.singtel.enums.Messages;

public class CanSwim implements Swim {
    public String swim() {
        return Messages.SWIM.getValue();
    }
}
