package com.shyam.singtel.abilities.swim;

import com.shyam.singtel.enums.Messages;

public class CantSwim implements Swim {
    public String swim() {
        return Messages.CANT_SWIM.getValue();
    }
}
