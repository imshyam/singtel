package com.shyam.singtel.abilities.kill;

import com.shyam.singtel.enums.Messages;

public class CantKillFish implements KillFish {
    public String killFish() {
        return Messages.CANT_EAT_OTHER_FISH.getValue();
    }
}
