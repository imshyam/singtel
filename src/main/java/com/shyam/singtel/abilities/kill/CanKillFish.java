package com.shyam.singtel.abilities.kill;

import com.shyam.singtel.enums.Messages;

public class CanKillFish implements KillFish {
    public String killFish() {
        return Messages.EAT_OTHER_FISH.getValue();
    }
}
