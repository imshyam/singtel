package com.shyam.singtel.abilities.sing;

import com.shyam.singtel.enums.Messages;

public class CantSing implements Sing {
    public String sing() {
        return Messages.CANT_SING.getValue();
    }
}
