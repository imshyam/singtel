package com.shyam.singtel.abilities.sing;

import com.shyam.singtel.enums.Messages;

public class CanSing implements Sing {

    String sound;

    public CanSing(String sound) {
        this.sound = sound;
    }

    public String sing() {
        return Messages.SING.getValue() + sound;
    }
}
