package com.shyam.singtel.abilities.walk;

import com.shyam.singtel.enums.Messages;

public class CanWalk implements Walk {
    public String walk() {
        return Messages.WALK.getValue();
    }
}
