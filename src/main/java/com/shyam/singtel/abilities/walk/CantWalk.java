package com.shyam.singtel.abilities.walk;

import com.shyam.singtel.enums.Messages;

public class CantWalk implements Walk {

    public String walk() {
        return Messages.CANT_WALK.getValue();
    }
}
