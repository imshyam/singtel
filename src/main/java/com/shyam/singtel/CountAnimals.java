package com.shyam.singtel;

import com.shyam.singtel.abilities.fly.CanFly;
import com.shyam.singtel.abilities.sing.CanSing;
import com.shyam.singtel.abilities.swim.CanSwim;
import com.shyam.singtel.abilities.walk.CanWalk;

import java.util.stream.Stream;

public class CountAnimals {

    int countFlying(Animal[] animals) {
        return (int)Stream.of(animals).filter(animal -> animal.getFly() instanceof CanFly).count();
    }

    public int countSwimming(Animal[] animals) {
        return (int)Stream.of(animals).filter(animal -> animal.getSwim() instanceof CanSwim).count();
    }

    public int countSinging(Animal[] animals) {
        return (int)Stream.of(animals).filter(animal -> animal.getSing() instanceof CanSing).count();
    }

    public int countWalking(Animal[] animals) {
        return (int)Stream.of(animals).filter(animal -> animal.getWalk() instanceof CanWalk).count();
    }
}
